import asyncio
import aioredis
import requests
from pydantic import BaseModel


class Measurement(BaseModel):
    device_id: str
    measurement: str
    timestamp: str


async def process_event(event):
    """
    It takes an event from the stream, decodes the data, creates a measurement object,
    and stores it in
    the database
    :param event: The event that was received from the stream
    """
    stream, data = event
    data = {k.decode(): v.decode() for k, v in data.items()}
    measurement = Measurement(**data)
    await store_measurement(measurement)


async def store_measurement(measurement: Measurement):
    """
    It sends a POST request to the administrator's API endpoint,
    which is responsible for storing the
    measurement in the database
    :param measurement: Measurement = Measurement(
    :type measurement: Measurement
    """
    url = "http://administartor:8000/measurement/add"
    data = measurement.dict()
    response = requests.post(url, json=data)
    print("🐍 File: Stock/main.py | Line: 41 | undefined ~ response", response)


async def consume_events(stream_name, group_name):
    """
    > Consume events from the stream, process them, and acknowledge them
    :param stream_name: The name of the stream to consume from
    :param group_name: The name of the consumer group
    """
    redis = await aioredis.from_url("redis://redis:6379")
    await redis.xgroup_destroy(stream_name, group_name)
    await redis.xgroup_create(stream_name, group_name, "$")
    while True:
        event = await redis.xreadgroup(group_name, "consumer1", streams={f'{stream_name}': '>'},
                                       count=None, block=0, noack=False)
        if event:
            await process_event(event[0][1][0])
        await redis.xack(stream_name, group_name, event[0][1][0][0])


async def main():
    """
    It creates a consumer group that subscribes to the `measurements` topic, and then it starts
    consuming events from that topic
    """
    await consume_events("measurements", "consumer_group")

if __name__ == "__main__":

    asyncio.run(main())
