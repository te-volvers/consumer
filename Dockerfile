FROM python:3.10
WORKDIR /code

COPY ./Consumer/requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./Consumer .


CMD ["python", "main.py"]

